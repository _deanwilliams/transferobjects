package com.dean.transferobjects;

import java.util.Date;

public interface City {

	public int getCityID();

	public String getCity();

	public int getCountryID();

	public Date getLastUpdate();
	
}
