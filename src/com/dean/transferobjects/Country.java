package com.dean.transferobjects;

import java.util.Date;

public interface Country {

	public int getCountryID();

	public String getCountry();

	public Date getLastUpdate();
	
}
