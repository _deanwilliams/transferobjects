package com.dean.transferobjects;

import java.io.Serializable;
import java.util.Date;

public class CityImpl implements City, Serializable {

	private static final long serialVersionUID = 1L;
	
	private int cityID;
	private String city;
	private int countryID;
	private Date lastUpdate;

	public CityImpl(int cityID, String city, int countryID, Date lastUpdate) {
		if (cityID <= 0) {
			throw new NullPointerException("city_id");
		}
		if (city == null) {
			throw new NullPointerException("city");
		}
		if (countryID <= 0) {
			throw new NullPointerException("country_id");
		}
		if (lastUpdate == null) {
			throw new NullPointerException("last_update");
		}
		this.cityID = cityID;
		this.city = city;
		this.countryID = countryID;
		this.lastUpdate = lastUpdate;
	}
	
	@Override
	public int getCityID() {
		return cityID;
	}

	@Override
	public String getCity() {
		return city;
	}

	@Override
	public int getCountryID() {
		return countryID;
	}

	@Override
	public Date getLastUpdate() {
		return lastUpdate;
	}

	@Override
	public String toString() {
		return getCity();
	}
}
