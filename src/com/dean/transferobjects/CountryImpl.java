package com.dean.transferobjects;

import java.io.Serializable;
import java.util.Date;

public class CountryImpl implements Country, Serializable {

	private static final long serialVersionUID = 1L;
	
	private int countryID;
	private String country;
	private Date lastUpdate;
	
	public CountryImpl(int countryID, String country, Date lastUpdate) {
		if (countryID <= 0) {
			throw new NullPointerException("country_id");
		}
		if (country == null) {
			throw new NullPointerException("country");
		}
		if (lastUpdate == null) {
			throw new NullPointerException("last_update");
		}
		this.countryID = countryID;
		this.country = country;
		this.lastUpdate = lastUpdate;
	}
	
	@Override
	public int getCountryID() {
		return countryID;
	}

	@Override
	public String getCountry() {
		return country;
	}

	@Override
	public Date getLastUpdate() {
		return lastUpdate;
	}

	@Override
	public String toString() {
		return country;
	}

}
